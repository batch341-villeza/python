#  Python List, Dictionary, Function, and Classes

# [Section 1] List
# Lists are similar to JS arrays
# To create a list, the square brackets([]) are used

names = ["John", "Paul", "George", "Ringo"]
durations = [760, 180, 20]
programs = ["Developer Career", "pi-shape", "short courses"]
truth_values = [True, False, True, True, False]
# print(names)
# print(programs)
# print(durations)
# print(truth_values)

sample_lists = ["Apple", 3, False, "Potato", 4, True]
# print(sample_lists)


# Getting the list size
# The number of elements in alist can be counted using the len() method
# print(len(programs))

# Accessing Values
# The indexes in the lists start at and ends at (n-1), where n is the number of elements
# Accessing first element in the list
# print(programs[0])

# Accessing the last element
# print(programs[-1])

# Accessing a range of values
# list_name[start index: end index]
# Note: The end index is not included
# print(programs[0:2]) # will display index 0 and 1

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# [Section] Mini exercise:
#	1. Create a list of names of 5 students
#	2. Create a list of grades for the 5 students
#	3. Use a loop to iterate through the lists printing in the following format:
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Solution # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
students = ["John", "Paul", "George", "Ringo", "Ben"]
grades = [85, 90, 75, 80, 82]

i = 0
for std in students:
	# print(f"The grade of {std} is {grades[i]}")
	i += 1

count = 0
while count < len(students):
    # print(f"The grade of {students[count]} is {grades[count]}")
    count += 1
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# [Section]
# Updating lists
# print(f"Current value: {programs[2]}")

# Update the value
programs[2] = "Short Courses"

# Print the new value
# print(f"New Value: {programs[2]}")

# List Manipulation
# Adding list items - append() method allows us to insert new item to a list
programs.append("global")
# print(programs)

# Deleting List Items - the del keyword can be used to delete elements in the list
durations.append(360)
# print(durations)

#  Delete the last item in the list
del durations[-1]
# print(durations)


# Membership checks - the "in" keyword checks if the element is in the list
# print(20 in durations) # True
# print(500 in durations) # False

# sort() - sorts the list alphabetically, ascending by default
names.sort()
# print(names)

# Emptying a list - the clear() method is used to empty the content of a list
test_list = [1,3,5,7,9]
# print(test_list)
test_list.clear()
# print(test_list)


# [Section 2] Dictionary "{}"
# Dictionaries are used to store data values in key:value pairs. This is similar to the object in JS.
person1 = {
	"name": "John",
	"age": 28,
	"occupation": "instructor",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}
# print(person1)

# To get the number of key-pairs in a dictionary, the len() method can be used
# print(len(person1))

#  Accessing values in a dictionary
#  To get the items in a dictionary, the key name can be refered using square brackets([])
# print(person1["name"])

# keys() - return all keys in the dictionary
# print(person1.keys())

# values() - return all the value in the ictionary
# print(person1.values())

# items() - return the key value pair in the dictionary
# print(person1.items())

# Adding key-value pairs
# person1["nationality"] = "Filipino"
# print(person1)

# person1.update({"fav_food":"Sinigang"})
# print(person1)

# deleting entries in a dictionary
# person1.pop("fav_food")
# print(person1)

# del person1["nationality"]
# print(person1)

# clear() method - empty a dictionary
# person2 = {
# 	"name": "Paul",
# 	"age": 28
# }
# print(person2)

# person2.clear()
# print(person2)

# Loop through a dictionary
# for key in person1:
# 	print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries
# person3 = {
# 	"name": "Monica",
# 	"age": 20,
# 	"occupation": "poet",
# 	"isEnrolled": True,
# 	"subjects": ["Python", "SQL", "Django"]
# }

# class_room = {
# 	"student1": person1,
# 	"student2": person2,
# 	"student3": person3,
# }

# print(class_room)

# {
# 	'student1': {
# 		'name': 'John',
# 		'age': 28,
# 		'occupation': 'instructor',
# 		'isEnrolled': True,
# 		'subjects': ['Python', 'SQL', 'Django']
# 	},
# 	'student2': {

# 	},
# 	'student3': {
# 		'name': 'Monica',
# 		'age': 20,
# 		'occupation': 'poet',
# 		'isEnrolled': True,
# 		'subjects': ['Python', 'SQL', 'Django']
# 	}
# }




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# [Section] Mini Exercise
#	1. Create a car dictionary with the following keys:
#		brand, model, year of make, color
#	2. Print the following statement from the details:
#		"I own a <Brand> <Model> and it was made in <Year of Make>"
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
car = {
	"brand": "Toyota",
	"model": "Corolla",
	"year_of_make": 1980,
	"color": "Red" 
}

# print(f"I own a {car['brand']} {car['model']} and it was made in {car['year_of_make']}")



# [Section] Pyhton Functions
# Functions are block of code that run run when called/invoked
# "def" keyword is used to define a function

# Syntax:
# 	def <function name>(parameter1, parameter2, parameter3...)
#		code block here


def my_greeting():
	# Code to be run when my_greeting() is invoked
	print("Hello, User!")

# Invoking a function - just specify the function name and provide an argument if needed
# my_greeting()


# Parameters can be added to functions to have more control to what the inputs for function will be
def greet_user(username):
	print(f"Hello, {username}!")

# Arguments are values that are provided to the function/substituteed to the parameters
# greet_user("Bob")
# greet_user("Amy")


# Return Statement	- "return" keyword allows functions to return values.
# 					-  this can be used to assign value in a variable.
def addition(num1, num2):
	return num1 + num2

sum = addition(5, 10)
# print(f"The sum is {sum}")


# [Section] Lambda Functions
# A lambda function is a small, anonymous function that can be used for callbacks
greeting = lambda person: f"Hello, {person}"
# print(greeting("Elsie"))
# print(greeting("Anthony"))

mult = lambda a, b: a * b
# print(mult(5, 6))
# print(mult(6, 99))


# Mini exercise # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
# 	Create a function that gets the square of a number
# 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def number(num):
	result = num ** 2
	return result

# while True:
# 	n = input("Enter a number: ")
# 	if n.isdigit():
# 		square = number(int(n))
# 		print(f"The square of {n} is {square}")
# 		break
# 	else:
# 		print("Invalid input.\n")



# [Section] Python Classes
# 	Classes serves as blueprints to describe the concept of objects
# 	Each object has characteristics(properties) and behaviors(methods)
# 	To create a class in python, the "class" keyword is used along with the class name that starts with an uppercase letter
# Syntax:
#	 class class_name():
#	 	def __init__(self, parameter1, parameter3, parameter3...):
#	 		self.parameter1 = parameter1
# 			...

# a. Create a Class
class Car():
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		self.fuel = "Gasoline"
		self.fuel_level = 0

	# methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print(f"Filling up the fuel tank...")

		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}\n")

	# Mini Exercise # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# 
	# 	1. Add a method called drive with a parameter called distance
	# 	2. The method would output 2 things
	# 		"The car has driven <distance> kilometers"
	# 		"The car's fuel level is <fuel level - distance>"
	# 
	# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

	# mini-activity solution # # # # # # # # # # # # # # # # # # # # # # # # #
	def drive(self, distance):
		self.distance = distance

		print(f"The car has driven {self.distance} kilometers")
		print(f"The car's fuel level is {self.fuel_level - self.distance}")


# Creating a new instance is done by calling the class and provideing the arguments
new_car = Car("Nissan", "GT-R", "2019")

# Displaying attributes can be done using dot notation
print(f"My car is a {new_car.brand} {new_car.model}\n")

# Calling a method of the instance
new_car.fill_fuel()

# Mini Activity: calling the class method
new_car.drive(40)
