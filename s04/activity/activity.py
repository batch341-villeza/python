# S04 Activity: # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 																							#
# 1. Create an abstract class called Animal that has the following abstract methods
# 																							#
#		Abstract Methods: eat(food), make_sound()
# 																							#
# 2. Create two classes that implements the Animal class called Cat and Dog
# 	 with each of the following properties and methods:
# 																							#
# 		Properties: name, breed, age
# 		Methods: getters and setters, implementation of abstract methods, call()
# 																							#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	@abstractclassmethod
	def make_sound(self):
		pass

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		print(f"Cat's name is {self._name}")

	def get_breed(self):
		print(f"Cat's breed is {self._breed}")

	def get_age(self):
		print(f"Cat's age is {self._age}")

	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed
		
	def set_age(self, age):
		self._age = age

	def eat(self, food):
		print(f"Serve me {food}!")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print("Puss, come on!")

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		print(f"Dog's name is {self._name}")

	def get_breed(self):
		print(f"Dog's breed is {self._breed}")

	def get_age(self):
		print(f"Dog's age is {self._age}")

	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed
		
	def set_age(self, age):
		self._age = age

	def eat(self, food):
		print(f"Eaten {food}!")

	def make_sound(self):
		print("Bark! Wook! Arf!")

	def call(self):
		print(f"Here {self._name}!")



pet1 = Dog("Isis", "Labrador", 1)
pet1.eat("Steak")
pet1.make_sound()
pet1.call()

pet2 = Cat("Sky", "Siamese", 3)
pet2.eat("Tuna")
pet2.make_sound()
pet2.call()
