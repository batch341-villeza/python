# Python Object Oriented Programming Fundamentals

# creating a simple class
class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")

my_Obj = SampleClass(2023)

# print(f"{my_Obj.year}")
# my_Obj.show_year()


# [Section] Fundamentals of OOP
# 	There are four main fundamentals principles of OOP
# 		a. Encapsulation
# 		b. Inheritance
# 		c. Polymorphism
# 		d. Abstraction

# a. Encapsulation
# 	- in Encapsulation, the attributes of a class will be hidden from other classes
# 	- to achieve Encapsulation:
# 		1. Declare the attibutes of a class
# 		2. Provide getter and setter methods to modify and view the attribute values
class Person():
	def __init__(self):
		# _name is a protected attribute
		self._name = "John Doe"
		self._age = 0

	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f"Name of person: {self._name}")

	def set_age(self, age):
		self._age = age

	def get_age(self):
		print(f"Age of person: {self._age}")

# Test Case 1:
# p1 = Person()
# p1.get_name()

# p1.set_name("Bob Doe")
# p1.get_name()

# print(p1.name) #

# Test Case 2:
# p1.get_age()

# p1.set_age(38)
# p1.get_age()



# b. Inheritance
# 	- 
# 	- 
class Employee(Person):
	def __init__(self, employee_id):
		super().__init__()
		self._employee_id = employee_id

	def get_employee_id(self):
		print(f"The Employee ID is: {self._employee_id}")

	def set_employee_id(self, employee_id):
		self._employee_id = employee_id

	def get_details(self):
		print(f"{self._employee_id} belongs to {self._name}")

# emp1 = Employee("Emp-001")
# emp1.get_details()
# emp1.set_name("Jane Doe")
# emp1.set_age(40)
# emp1.get_age()
# emp1.get_details()


# Mini exercise
# 	1. Create a new class called Student that inherits Person with the additional attributes and methods
#		attributes:
# 			Student No, Course, Year Level
#		methods: 
#       	get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>"

class Student(Person):
	def __init__(self, student_no, course, year_level):
		super().__init__()
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	# getter
	def get_student_no(self):
		print(f"Student number is: {self._student_no}")

	def get_course(self):
		print(f"Course of student is: {self._course}")

	def get_year_level(self):
		print(f"The year level of the student is: {self._course}")

	def get_detail(self):
		print(f"{self._name} is currently in year {self._year_level} taking up {self._course}")

	# setter
	def set_student_no(self, student_no):
		self._student_no = student_no

	def set_course(self, course):
		self._course = course

	def set_year_level(self, year_level):
		self._year_level = year_level

# solution
# std1 = Student("std-001", "BSIT", 3)
# std1.get_detail()

# Test Case
# std1.set_name("Brandon Smith")
# std1.set_age(18)
# std1.get_detail()



# c. Polymorphism
# 	- Polymorphism poly(many) morphism(forms)
#   - we can use functions, objects, classes to do polymorphism
# 	
# 	1. Function and Objects
# 		- 

class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")


class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Regular User")

# Define a test_function that will take an objects
def test_func(obj):
	obj.is_admin()
	obj.user_type()


# Instances for admin and customer
user_admin = Admin()
user_customer = Customer()


# Pass the created instances to test_func
# test_func(user_admin)
# test_func(user_customer)

# The test_func would call the methds of the object passed to it, hence allowing it to have different outputs based on the objects.

# 	2. Polymorphism with Calss Methods
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	# access the occupation method of each item
	# person.occupation()
	pass


#	3. Polymorphism with Inheritance
class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks(developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	# this overides the num_of_hours method from the parent class
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapeCareer(Zuitt):
	# this overides the num_of_hours method from the parent class
	def num_of_hours(self):
		print("Learn skills fo no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# this overides the num_of_hours method from the parent class
	def num_of_hours(self):
		print("Learn advance topics in web development in 20 hours!")


course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()

# course1.num_of_hours()
# course2.num_of_hours()
# course3.num_of_hours()

'''
	d. Abstraction
		- An abstract class can be considered as a blueprint for other classes. It allows you to create a set of methods that must be created within any child classes built from the abstract class. 
		- A class which contains one or more abstract methods is called an abstract class.
		- An abstract method is a method that has a declaration but does not have an implementation.
		- Abstract classes are used to provide a common interface for different implementations for different classes.
		- By default, Python does not provide abstract classes. Python comes with a module that provides the base for defining "Abstract Base classes" (ABC) and that module name is ABC
'''

# The import tells the program to get the abc module of python to be used.
from abc import ABC, abstractclassmethod 

# The class Polygon inherits the abstract class module
class Polygon(ABC):

    # Create an abstract method called printNumberOfSides that needs to be implemented by classes that inherit Polygon
    # this @abstractclassmethod is a decorator
    @abstractclassmethod
    def printNumberOfSides(self):
        # The pass keyword denotes that the method doesn't do anything.
        pass

class Triangle(Polygon):
    def __init__(self):
        super().__init__()
    
    # Since the Triangle class inherited the Polygon class, it must now implement the abstract method
    def printNumberOfSides(self):
        print(f"This polygon has 3 sides.")


class Pentagon(Polygon):
    def __init__(self):
        super().__init__()

    # Since the Pentagon class inherited the Polygon class, it must now implement the abstract method
    # def printNumberOfSides(self):
    #     print(f"This polygon has 5 sides.")

shape1 = Triangle()
shape2 = Pentagon()
shape1.printNumberOfSides()
shape2.printNumberOfSides()

# shape = Polygon()
# shape.printNumberOfSides()