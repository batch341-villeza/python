# Capstone:

'''
Specifications

1. Create a Person class that is an abstract class that has the following methods
	a. getFullName method
	b. addRequest method
	c. checkRequest method
	d. addUser method
'''
from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self):
		pass

	@abstractclassmethod
	def addRequest(self):
		pass

	@abstractclassmethod
	def checkRequest(self):
		pass

	@abstractclassmethod
	def addUser(self):
		pass

'''
2. Create an Employee class from Person with the following properties and methods
	a. Properties(make sure they are private and have getters/setters)
		firstName,
		lastName,
		email,
		department

	b. Methods
		Abstract methods
			> For the checkRequest and addUser methods, they should do nothing.
			> login() - outputs "<Email> has logged in"
			> logout() - outputs "<Email> has logged out"
			
		Note: All methods just return Strings of simple text
			ex. Request has been added
'''
class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# getter
	def get_first_name(self):
		return self._firstName

	def get_last_name(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	# setter
	def set_first_name(self, first_name):
		self._firstName = first_name

	def set_last_name(self, last_name):
		self._lastName = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	# abstract method
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return "Request has been added"

	def checkRequest(self):
		pass
		
	def addUser(self):
		pass

	# methods
	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return  f"{self._email} has logged out"


'''
3. Create a TeamLead class from Person with the following properties and methods
	a. Properties(make sure they are private and have getters/setters)
		firstName,
		lastName,
		email,
		department,
		members

	b. Methods
		Abstract methods
			> For the addRequest and addUser methods, they should do nothing.
			> login() - outputs "<Email> has logged in"
			> logout() - outputs "<Email> has logged out"
			> addMember() - adds an employee to the members list

		Note: All methods just return Strings of simple text
			ex. Request has been added
'''
class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self._members = []

	# getter
	def get_first_name(self):
		return self._firstName

	def get_last_name(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def get_members(self):
		return self._members

	# setter
	def set_first_name(self, first_name):
		self._firstName = first_name

	def set_last_name(self, last_name):
		self._lastName = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	# abstract method
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		pass

	def checkRequest(self):
		pass
		
	def addUser(self):
		pass

	# methods
	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return  f"{self._email} has logged out"

	def addMember(self, employee):
		self._members.append(employee)


'''
4. Create an Admin class from Person with the following properties and methods
	a. Properties(make sure they are private and have getters/setters)
		firstName,
		lastName,
		email,
		department

	b. Methods
		Abstract methods
			> For the checkRequest and addRequest methods, they should do nothing.
			> login() - outputs "<Email> has logged in"
			> logout() - outputs "<Email> has logged out"
			> addUser() - outputs "New user added"

		Note: All methods just return Strings of simple text
			ex. Request has been added
'''
class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# getter
	def get_first_name(self):
		return self._firstName

	def get_last_name(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	# setter
	def set_first_name(self, first_name):
		self._firstName = first_name

	def set_last_name(self, last_name):
		self._lastName = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	# abstract method
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		pass

	def checkRequest(self):
		pass
		
	def addUser(self):
		return "User has been added"

	# methods
	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return  f"{self._email} has logged out"


'''
5. Create a Request that has the following properties and methods
	a. properties
		name,
		requester,
		dateRequested,
		status

	b. Methods
			updateRequest
			closeRequest
			cancelRequest
		Note: All methods just return Strings of simple text
			Ex. Request < name > has been updated/closed/cancelled
'''
class Request():
	def __init__(self, name, requester, dateRequested):
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "Open"

	# getter
	def get_name(self):
		return self._name

	def get_requester(self):
		return self._requester

	def get_dateRequested(self):
		return self._dateRequested

	def get_status(self):
		return self._status

	# setter
	def set_name(self, name):
		return self._name

	def set_requester(self, requester):
		return self._requester

	def set_dateRequested(self, dateRequested):
		return self._dateRequested

	def set_status(self, status):
		return self._status

	# methods
	def updateRequest(self):
		return f"Request {self._name} has been updated!"

	def closeRequest(self):
		return f"Request {self._name} has been closed!"

	def cancelRequest(self):
		return f"Request {self._name} has been cancelled!"


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")

# print(employee1.get_first_name())
# print(employee1.get_last_name())
# print(employee1.get_email())
# print(employee1.get_department())
# print(employee1.getFullName())
# print(employee1.addRequest())
# print(employee1.login())
# print(employee1.logout())

teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

# print(teamLead1.get_first_name())
# print(teamLead1.get_last_name())
# print(teamLead1.get_email())
# print(teamLead1.get_department())
# print(teamLead1.getFullName())
# print(teamLead1.get_members())
# print(teamLead1.login())
# print(teamLead1.logout())

admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")

# print(admin1.get_first_name())
# print(admin1.get_last_name())
# print(admin1.get_email())
# print(admin1.get_department())
# print(admin1.getFullName())
# print(admin1.addUser())
# print(admin1.login())
# print(admin1.logout())

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

# print(req1.get_name())
# print(req1.get_requester().getFullName())
# print(req1.get_dateRequested())
# print(req1.get_status())

# print(req2.get_name())
# print(req2.get_requester().getFullName())
# print(req2.get_dateRequested())
# print(req2.get_status())


# Checking # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert admin1.addUser() == "User has been added"

req2.set_status("Closed")
print(req2.closeRequest())

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #