# S01 Activity:

# 1. Create 5 variables and output them in the command prompt in the following format:
# 
# "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

# Solution:
name = "Braian"
age = 27
occupation = ""
movie_title = "Avatar: The Way Of Water"
movie_rating = 99.99

print(f"I am {name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie_title} is {movie_rating} %")



# 2. Create 3 variables, num1, num2, and num3
#	 a. Get the product of num1 and num2
#	 b. Check if num1 is less than num3
#	 c. Add the value of num3 to num2

# Solution:
num1 = 3
num2 = 5
num3 = 2

print(num1 * num2)
print(num1 < num3)

# num3 += num2
# print(num3)
# or
print(num3 + num2)