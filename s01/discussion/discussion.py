# Python Course

# [Section I] Python Comments
# Comments in Python are done using the "# symbol


# [Section II] Python Syntax
print('Hello World!');

# [Section III] Indentation
# Indentation in Python is very important is very important. It is used to indicate a block of code.


# [Section IV] Python Variables
# The terminology used for variable names is "identifier".
# Python variable is declared by stating the variable name and assigning a value using the equality symbol.

# Variable Naming Convention
# Python uses the "snake case" naming convention
age = 35
print(age)

middle_initial = "C"
print(middle_initial)

# Python allows assigning of values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1)



# [Section V] Python Data Types
# 1. String (str) - for alphanumeric and symbols
full_name = "Braian"
print(full_name)

password = "pa$$word"
print(password)

# 2. Numbers (int, float, complex) - for intergers, decimal, and complex numbers.
num_of_days = 365
print(num_of_days)

pi_approx = 3.1416
print(pi_approx)

complex_num = 1 + 5j
print(complex_num)
print(complex_num.real)
print(complex_num.imag)

# 3. Boolean
is_learning = True
is_difficult = False
print(is_learning)
print(is_difficult)



# [Section VI] Using Variables
# Just like JS, variables are used by simply calling the name of the identifiers.
# To use variables, concatenation ("+") symbol betweetn strings can be used.
print("My name is "+full_name) 

# variables with the data type of string cannot be concatenante with an number datatype.
# print("My age is " + age) # TypeError: can only concatenate str (not "int") to str
# To concatenate this we need to cast the number data type to string
print("My age is " + str(age))

# [Section VII] Type Casting
# There may be times when we want to specify a type on to a variable. This can be done with casting. Here are some function that can be used:
# 1. int()
# 2. float()
# 3. str()
print(int(3.5))
print(float(3.5))
print(str(3.5))

# f String - this is another way to concatenate string with different variable data types.
print(f"My name is {full_name} and my age is {age}.")


# [Section 8] Operations
# Python has operator families that can be used to manipulate variables

# 1. Arithmetic Operators - perform mathematical operations
# 	a. addition
print(1 + 10)

# 	b. subtraction
print(15- 8)

# 	c. multiplication
print(18 * 9)

# 	d. division
print(21 / 7)

# 	e. floor division
print(18 // 4)	

# 	f. modulo
print(18 % 4)

# 	g. exponential
print(2 ** 6)


# 2. Assignment Operators - used to assign values to a variable
num = 1
# a. +=
print()
# b. -=
# c. *=
# d. /=
# e. //=
# f. %=


# 3. Comparison Operator
print(1 == 1)
print(1 == "1")
# other comparison operator (!=, <, <=, >, >=)



# 4. Logical Operators
# and not or
print(True and False)
print(not False)
print(True or False)
