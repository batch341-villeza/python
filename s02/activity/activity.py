# S02 Activity: #####################################################################################################
# 	1. Get a year from the user and determine if it is a leap year or not.											#
# 	2. Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.	#
#																													#
# 	Stretch goal: Implement error checking for the leap year inputs													#
# 		1. Strings are not allowed for inputs																		#
# 		2. No zero or negative values																				#
#####################################################################################################################


# 1.
while True:
	year = input("Enter a year: ")
	is_number = year.isdigit()
	if is_number:
		y = int(year)
		if y > 0:
			if (y % 4) == 0:
				print(f"Year {y} is a Leap Year!\n")
				break
			else:
				print(f"Year {y} is NOT a Leap Year!\n")
				break
		else:
			print("Please enter a valid year!\n")
	else:
		print("Sorry, you have entered a invalid value!\n")


# 2.
while True:
	num_rows = input("Enter number of rows: ")
	num_cols = input("Enter number of columns: ")

	if num_rows.isdigit() and num_cols.isdigit():
		row = int(num_rows)
		col = int(num_cols)
		if row > 0 and col > 0:
			for r in range(row):
				for c in range(col):
					print("* ", end="")
				print()
			break
	else:
		print("Please enter a valid number of rows and columns!\n")