# username = input("Please enter your name: ")
# print(f"Hello {username}! Welcome to Python Short Course!")

# Input automatically assigns input as string data type. To solve this, we can convert the data type to number using the int() method.

# num1 = int(input("Enter a number: "))
# num2 = int(input("Enter a number: "))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# [Section 2] If-else statements
# if-else statements are used for the conditional statement
# test_num = 75

# if test_num >= 60:
# 	print("Test Passed")
# else:
# 	print("Test Failed")

# 	Note: Indentations are important in Python as it uses identations to distinguise parts of code.

# test_num2 = int(input("Please enter the second text number: "))

# if test_num2 > 0:
# 	print("The number is positive.")
# elif test_num2 == 0:
# 	print("The number is zero.")
# else:
# 	print("The number is negative")




#############################################################################################
# Mini Exercise 1:																			#
#	Create an if-else statement that determines if a number is divisible by 3, 5, or both. 	#
#	If the number is divisible by 3, print "The number is divisible by 3"					#
#	If the number is divisible by 5, print "The number is divisible by 5"					#
#	If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"	#
#	If the number is not divisible by any, print "The number is not divisible by 3 nor 5"	#
#############################################################################################

# num = int(input("Enter a number: "))

# if (num % 3) == 0 and (num % 5) == 0:
# 	print("The number is divisible by both 3 and 5")
# elif (num % 3) == 0:
# 	print("The number is divisible by 3")
# elif (num % 5) == 0:
# 	print("The number is divisible by 5")
# else:
# 	print("The number is not divisible by 3 nor 5")


# [Section ] Python Loop
# a. While Loop
# i = 1
# while i <= 5:
# 	print(f"Current count: {i}")
# 	i += 1

# b. Foor Loop
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)

# Loop using Python range() method
# syntax:
# 	range(stop) - range with single argument determines the
# 	range(start, stop, [step])
# for x in range(6):
# 	print(f"The current value is {x}")

# for x in range(6, 10):
# 	print(f"The current value is {x}")

# for x in range(6, 20, 2):
# 	print(f"The current value is {x}")


# Mini Exercise 2:
# Create a loop to count and display the number of even numbers between 1 and 20.
# Print "The number of even numbers between 1 and 20 is: <number of even numbers"

# nums = range(1, 21)
# count = 0
# for num in nums:
# 	if (num % 2) == 0:
# 		count += 1
# print(f"The total count of even numbers between 1 and 20 is: {count}")

# Mini Exercise 3:
    # Write a Python program that takes an integer input from the user and displays the multiplication table for that number, from 1 to 10.

# input_num = int(input("Enter a number: "))
# for num in range(1, 11):
# 	result = input_num*num
# 	print(f"{input_num} x {num} = {result}")

# [Section ] Loop Statement
# a. break statement
# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1
# When j reaches 3, the loop ends the next iterations are not run


# b. Continue Statement
# k = 1
# while k < 6:
# 	k += 1
# 	if k == 3:
# 		continue
# 	print(k)